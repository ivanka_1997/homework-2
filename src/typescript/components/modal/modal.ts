import { createElement } from "../../helpers/domHelper";

interface modalParams {
  title: string;
  bodyElement: HTMLDivElement;
  onClose: () => void;
}

export function showModal({
  title,
  bodyElement,
  onClose = () => {},
}: modalParams) {
  const root: HTMLDivElement = getModalContainer();
  const modal: HTMLDivElement = createModal({ title, bodyElement, onClose });

  root.append(modal);
}

function getModalContainer() {
  return document.getElementById("root") as HTMLDivElement;
}

function createModal({
  title,
  bodyElement,
  onClose,
}: modalParams): HTMLDivElement {
  const layer: HTMLDivElement = createElement({
    tagName: "div",
    className: "modal-layer",
  });
  const modalContainer: HTMLDivElement = createElement({
    tagName: "div",
    className: "modal-root",
  });
  const header: HTMLDivElement = createHeader(title, onClose);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string, onClose: () => void): HTMLDivElement {
  const headerElement: HTMLDivElement = createElement({
    tagName: "div",
    className: "modal-header",
  });
  const titleElement: HTMLDivElement = createElement({
    tagName: "span",
    className: "close-span",
  });
  const closeButton: HTMLDivElement = createElement({
    tagName: "div",
    className: "close-btn",
  });

  titleElement.innerText = title;
  closeButton.innerText = "×";

  const close = () => {
    hideModal();
    onClose();
  };
  closeButton.addEventListener("click", close);
  headerElement.append(titleElement, closeButton);

  return headerElement;
}

function hideModal() {
  const modal: HTMLDivElement = <HTMLDivElement>(
    document.getElementsByClassName("modal-layer")[0]
  );
  modal?.remove();
}
