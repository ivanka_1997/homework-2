import { showModal } from "./modal";
import { createElement } from "../../helpers/domHelper";
import { Fighter } from "../../models/fighter.model";

export function showWinnerModal(fighter: Fighter) {
  // call showModal function
  const title = `${fighter.name} is the winner!`;
  const bodyElement: HTMLDivElement = createElement({
    tagName: "span",
    className: "modal_close",
  });
  bodyElement.innerText = `Close the window to start a new game.`;
  const onClose = () => window.location.reload();
  showModal({ title, bodyElement, onClose });
}
