import { createElement } from "../helpers/domHelper";
import { createFighterImage } from "./fighterPreview";
import { fight } from "./fight";
import { showWinnerModal } from "./modal/winner";
import { Fighter } from "../models/fighter.model";

export async function renderArena(selectedFighters: [Fighter, Fighter]) {
  const root: HTMLElement = <HTMLElement>document.getElementById("root");
  const arena: HTMLDivElement = createArena(selectedFighters);

  root.innerHTML = "";
  root.append(arena);
  // - start the fight
  const winner: Fighter = await fight(...selectedFighters);
  // - when fight is finished show winner
  showWinnerModal(winner);
}

function createArena(selectedFighters: [Fighter, Fighter]): HTMLDivElement {
  const arena: HTMLDivElement = createElement({
    tagName: "div",
    className: "arena___root",
  });
  const healthIndicators: HTMLDivElement = createHealthIndicators(
    ...selectedFighters
  );
  const fighters: HTMLDivElement = createFighters(...selectedFighters);

  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(
  leftFighter: Fighter,
  rightFighter: Fighter
): HTMLDivElement {
  const healthIndicators: HTMLDivElement = createElement({
    tagName: "div",
    className: "arena___fight-status",
  });
  const versusSign: HTMLDivElement = <HTMLDivElement>createElement({
    tagName: "div",
    className: "arena___versus-sign",
  });
  const leftFighterIndicator: HTMLDivElement = createHealthIndicator(
    leftFighter,
    "left"
  );
  const rightFighterIndicator: HTMLDivElement = createHealthIndicator(
    rightFighter,
    "right"
  );

  healthIndicators.append(
    leftFighterIndicator,
    versusSign,
    rightFighterIndicator
  );
  return healthIndicators;
}

function createHealthIndicator(
  fighter: Fighter,
  position: "left" | "right"
): HTMLDivElement {
  const { name } = fighter;
  const container: HTMLDivElement = createElement({
    tagName: "div",
    className: "arena___fighter-indicator",
  });
  const fighterName: HTMLSpanElement = createElement({
    tagName: "span",
    className: "arena___fighter-name",
  });
  const indicator: HTMLDivElement = createElement({
    tagName: "div",
    className: "arena___health-indicator",
  });
  const bar: HTMLDivElement = createElement({
    tagName: "div",
    className: "arena___health-bar",
    attributes: { id: `${position}-fighter-indicator` },
  });

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(
  firstFighter: Fighter,
  secondFighter: Fighter
): HTMLDivElement {
  const battleField: HTMLDivElement = createElement({
    tagName: "div",
    className: `arena___battlefield`,
  });
  const firstFighterElement: HTMLDivElement = createFighter(
    firstFighter,
    "left"
  );
  const secondFighterElement: HTMLDivElement = createFighter(
    secondFighter,
    "right"
  );

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(
  fighter: Fighter,
  position: "left" | "right"
): HTMLDivElement {
  const imgElement: HTMLImageElement = createFighterImage(fighter);
  const positionClassName: string =
    position === "right" ? "arena___right-fighter" : "arena___left-fighter";
  const fighterElement: HTMLDivElement = createElement({
    tagName: "div",
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
