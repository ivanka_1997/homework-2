import { controls } from "../../constants/controls";
import { Fighter } from "../models/fighter.model";

export async function fight(
  firstFighter: Fighter,
  secondFighter: Fighter
): Promise<Fighter> {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    let keysSet: Set<string> = new Set<string>();
    // PlayerOneAttack: 'KeyA',
    // PlayerOneBlock: 'KeyD',
    // PlayerTwoAttack: 'KeyJ',
    // PlayerTwoBlock: 'KeyL',
    // PlayerOneCriticalHitCombination: ['KeyQ', 'KeyW', 'KeyE'],
    // PlayerTwoCriticalHitCombination: ['KeyU', 'KeyI', 'KeyO']
    const keys: Array<string> = [
      controls.PlayerOneAttack[0],
      controls.PlayerOneBlock[0],
      controls.PlayerTwoAttack[0],
      controls.PlayerTwoBlock[0],
      controls.PlayerOneCriticalHitCombination[0],
      controls.PlayerOneCriticalHitCombination[1],
      controls.PlayerOneCriticalHitCombination[2],
      controls.PlayerTwoCriticalHitCombination[0],
      controls.PlayerTwoCriticalHitCombination[1],
      controls.PlayerTwoCriticalHitCombination[2],
    ];

    let firstPlayer = Object.assign({}, firstFighter);
    let secondPlayer = Object.assign({}, secondFighter);

    let isCriticalHitFirstActive: boolean = true;
    let isCriticalHitSecondActive: boolean = true;

    let firstPlayerisCriticalHit: boolean = false;
    let secondPlayerisCriticalHit: boolean = false;

    const firstHealthIndicator: HTMLElement = <HTMLElement>(
      document.getElementById("left-fighter-indicator")
    );
    const secondHealthIndicator: HTMLElement = <HTMLElement>(
      document.getElementById("right-fighter-indicator")
    );

    document.addEventListener("keydown", function (e) {
      if (firstPlayer.health > 0 && secondPlayer.health > 0) {
        if (keys.includes(e.code)) {
          keysSet.add(e.code);
        }

        if (keysSet.size == 1) {
          if (keysSet.has(keys[0])) {
            firstPlayer.attack = getHitPower(firstFighter);
            let attack = getHitPower(firstFighter);
            secondPlayer.health -= attack;

            secondHealthIndicator.style.width = `${
              (secondPlayer.health * 100) / secondFighter.health
            }%`;
          }
          if (keysSet.has(keys[1])) {
            firstPlayer.defense = getBlockPower(firstFighter);
          }
          if (keysSet.has(keys[2])) {
            secondPlayer.attack = getHitPower(secondFighter);
            firstPlayer.health -= secondPlayer.attack;
            firstHealthIndicator.style.width = `${
              (firstPlayer.health * 100) / firstFighter.health
            }%`;
          }
          if (keysSet.has(keys[3])) {
            secondPlayer.defense = getBlockPower(secondFighter);
          }
        }

        if (
          (keysSet.has(keys[0]) && keysSet.has(keys[1])) ||
          (keysSet.has(keys[2]) && keysSet.has(keys[3]))
        ) {
          console.log("cant block and attack");
        }
        if (keysSet.has(keys[0]) && keysSet.has(keys[3])) {
          if (keysSet.entries().next().value[0] == keys[3]) {
            console.log("block is equel or bigger then attack");
          } else {
            let damage = getDamage(firstFighter, secondFighter);
            secondPlayer.health -= damage;

            secondHealthIndicator.style.width = `${
              (secondPlayer.health * 100) / secondFighter.health
            }%`;
          }
        }
        if (keysSet.has(keys[1]) && keysSet.has(keys[2])) {
          if (keysSet.entries().next().value[0] == keys[1]) {
            console.log("block is equel or bigger then attack");
          } else {
            let damage = getDamage(secondFighter, firstFighter);
            firstPlayer.health -= damage;
            firstHealthIndicator.style.width = `${
              (firstPlayer.health * 100) / firstFighter.health
            }%`;
          }
        }

        ///critical
        if (keysSet.size == 3) {
          if (
            keysSet.has(keys[4]) &&
            keysSet.has(keys[5]) &&
            keysSet.has(keys[6]) &&
            isCriticalHitFirstActive
          ) {
            firstPlayer.attack = firstFighter.attack * 2;
            secondPlayer.health -= firstPlayer.attack;

            secondHealthIndicator.style.width = `${
              (secondPlayer.health * 100) / secondFighter.health
            }%`;

            isCriticalHitFirstActive = false;
            firstPlayerisCriticalHit = true;
            setTimeout(() => {
              isCriticalHitFirstActive = true;
            }, 10000);
          }

          if (
            keysSet.has(keys[7]) &&
            keysSet.has(keys[8]) &&
            keysSet.has(keys[9]) &&
            isCriticalHitSecondActive
          ) {
            secondPlayer.attack = secondFighter.attack * 2;

            firstPlayer.health -= secondPlayer.attack;
            firstHealthIndicator.style.width = `${
              (firstPlayer.health * 100) / firstFighter.health
            }%`;

            isCriticalHitSecondActive = false;
            secondPlayerisCriticalHit = true;
            setTimeout(() => {
              isCriticalHitSecondActive = true;
            }, 10000);
          }
          if (firstPlayerisCriticalHit) secondPlayer.defense = 0;
          if (secondPlayerisCriticalHit) firstPlayer.defense = 0;
        }
      }
      if (firstPlayer.health <= 0) resolve(secondFighter);
      if (secondPlayer.health <= 0) resolve(firstFighter);
    });

    document.addEventListener("keyup", function (e) {
      if (keys.includes(e.code)) {
        keysSet.delete(e.code);
      }
    });
  });
}
export function getDamage(attacker: Fighter, defender: Fighter): number {
  // return damage
  let demage: number = getHitPower(attacker) - getBlockPower(defender);
  return demage <= 0 ? 0 : demage;
}

export function getHitPower(fighter: Fighter): number {
  // return hit power
  let criticalHitChance: number = Math.random() + 1;
  let power: number = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter: Fighter): number {
  // return block power
  let dodgeChance: number = Math.random() + 1;
  let power: number = fighter.defense * dodgeChance;
  return power;
}
